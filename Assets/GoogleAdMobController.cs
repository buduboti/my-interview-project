﻿using UnityEngine.Events;
using UnityEngine;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class GoogleAdMobController : MonoBehaviour
{
    public static GoogleAdMobController _instance;

    public static GoogleAdMobController Instance {
        get {
            if (_instance == null) {
                _instance = new GoogleAdMobController();
            }
            return _instance;
        }
    }

    private InterstitialAd interstitialAd;
    private RewardedAd rewardedAd;
 
    void Start() {
        MobileAds.Initialize(initStatus => { });
         // RequestAndLoadRewardedAd();
    }

    #region HELPER METHODS

    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)
            .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
            .AddKeyword("unity-admob-sample")
            .TagForChildDirectedTreatment(false)
            .AddExtra("color_bg", "9B30FF")
            .Build();
    }

    public bool rewardedAdIsLoaded() {
        RequestAndLoadRewardedAd();
        return (rewardedAd != null);
    }

    public bool interstitialAdIsLoaded() {
        RequestAndLoadInterstitialAd();
        return interstitialAd.IsLoaded();
    }

    public void rewardedAdShow() {
        rewardedAd.Show();
    }

    public void interstitialAdShow() {
        interstitialAd.Show();
    }

    #endregion

    #region INTERSTITIAL ADS

    public void RequestAndLoadInterstitialAd()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-3600355124970273/5055263826";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Clean up interstitial before using it
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }

        interstitialAd = new InterstitialAd(adUnitId);

        // Load an interstitial ad
        interstitialAd.LoadAd(CreateAdRequest());
    }

    public void ShowInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    	RequestAndLoadInterstitialAd();
        interstitialAd.Show();
    }

    public void DestroyInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    }
    #endregion

    #region REWARDED ADS

    public void RequestAndLoadRewardedAd()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-3600355124970273/2604005084";
#else
        string adUnitId = "unexpected_platform";
#endif

        // create new rewarded ad instance
        rewardedAd = new RewardedAd(adUnitId);

        // Create empty ad request
        rewardedAd.LoadAd(CreateAdRequest());
    }

    public void ShowRewardedAd()
    {
    	RequestAndLoadRewardedAd();
        rewardedAd.Show();    
    }

    #endregion
}