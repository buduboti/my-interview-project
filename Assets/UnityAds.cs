﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAds : MonoBehaviour, IUnityAdsListener
{
    public static UnityAds _instance;

    public static UnityAds Instance {
        get {
            if (_instance == null) {
                _instance = new UnityAds();
            }
            return _instance;
        }
    }

	string gameId = "3935969";
    string myPlacementId = "rewardedVideo";
    bool testMode = true;
    
    // Start is called before the first frame update
    void Start()
    {
        Advertisement.AddListener (this);
        Advertisement.Initialize (gameId, testMode);
    }

    public bool ShowRewardedVideo() {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady(myPlacementId)) {
            Advertisement.Show(myPlacementId);
            return true;
        } 
        else {
        	return false;
        }
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish (string placementId, ShowResult showResult) {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished) {
            // Reward the user for watching the ad to completion.
        } else if (showResult == ShowResult.Skipped) {
            // Do not reward the user for skipping the ad.
        } else if (showResult == ShowResult.Failed) {
            Debug.LogWarning ("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsReady (string placementId) {
        // If the ready Placement is rewarded, show the ad:
        if (placementId == myPlacementId) {
            // Optional actions to take when the placement becomes ready(For example, enable the rewarded ads button)
        }
    }

    public void OnUnityAdsDidError (string message) {
        // Log the error.
    }

    public void OnUnityAdsDidStart (string placementId) {
        // Optional actions to take when the end-users triggers an ad.
    } 

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy() {
        Advertisement.RemoveListener(this);
    }

    public bool ShowInterstitialAd() {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady()) {
            Advertisement.Show();
            return true;
        } 
        else {
        	return false;
        }
    }

    public bool rewardedAdIsLoaded() {
        return Advertisement.IsReady(myPlacementId);
    }

    public bool interstitialAdIsLoaded() {
        return Advertisement.IsReady();
    }

    public void rewardedAdShow() {
        Advertisement.Show(myPlacementId);
    }
    
    public void interstitialAdShow() {
        Advertisement.Show();
    }
}
