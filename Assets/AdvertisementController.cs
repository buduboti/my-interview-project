﻿using System;
using System.IO;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;
using UnityEngine.Analytics;

public class AdvertisementController : MonoBehaviour
{
	GoogleAdMobController adMob = GoogleAdMobController.Instance;
	SuperAwesomeAds saa = SuperAwesomeAds.Instance;
	UnityAds unityAds = UnityAds.Instance;

	List<string> adProviders;

	void Start() {
		// adProviders = new List<string>();

		// adProviders.Add("SuperAwesomeAds");
		// adProviders.Add("AdMob");
		// adProviders.Add("UnityAds");

		string json = string.Empty;
		string url = @"https://bbbinterview.fra1.digitaloceanspaces.com/providerLine.json";

		HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
		request.AutomaticDecompression = DecompressionMethods.GZip;

		using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
		using (Stream stream = response.GetResponseStream())
		using (StreamReader reader = new StreamReader(stream))
		{
		    json = reader.ReadToEnd();
		}

		json = Regex.Replace(json, @"\s+", "");

		Debug.Log(json);
		json = json.Substring(1);
		Debug.Log(json);
		json = json.Substring(0, json.Length-1);
		Debug.Log(json);

		string[] adProvidersFromJson = json.Split(',')
			.Select(prov => prov.Substring(1)).ToArray()
			.Select(prov => prov.Substring(0, prov.Length-1)).ToArray();

		adProviders = adProvidersFromJson.OfType<string>().ToList();

	}

	public void interstitialClickAction() {
    	Analytics.CustomEvent("interstitialAdButtonClicked");
		Debug.Log("in InterstitialClickAction");
		foreach (string provider in adProviders) {
			Debug.Log("in foreach | provider = " + provider);
			switch (provider)
			{
				case "UnityAds":
					if (unityAds.ShowInterstitialAd()) {
						return;
					} else {
						continue;
					}
				case "AdMob":
					if (adMob.interstitialAdIsLoaded()) {
						adMob.interstitialAdShow();
						return;
					} else {
						continue;
					}
				case "SuperAwesomeAds":
					if (saa.interstitialAdIsLoaded()) {
						saa.interstitialAdShow();
						return;
					} else {
						continue;
					}
				default:
					Debug.Log("Unknown provider: " + provider);
					break;
			}
		}
		Debug.Log("Interstitial ads are not ready yet.");
	}

	public void rewardClickAction() {
    	Analytics.CustomEvent("rewardAdButtonClicked");
		foreach (string provider in adProviders) {
			switch (provider)
			{
				case "UnityAds":
					if (unityAds.rewardedAdIsLoaded()) {
						unityAds.rewardedAdShow();
						return;
					} else {
						continue;
					}
				case "AdMob":
					if (adMob.rewardedAdIsLoaded()) {
						adMob.rewardedAdShow();
						return;
					} else {
						continue;
					}
				case "SuperAwesomeAds":
					if (saa.rewardedAdIsLoaded()) {
						saa.rewardedAdShow();
						return;
					} else {
						continue;
					}
				default:
					Debug.Log("Unknown provider: " + provider);
					break;
			}
		}
		Debug.Log("Rewarded ads are not ready yet.");
	}
}
