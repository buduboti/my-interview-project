﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using tv.superawesome.sdk.publisher;

public class SuperAwesomeAds : MonoBehaviour
{
	public static SuperAwesomeAds _instance;

    public static SuperAwesomeAds Instance {
        get {
            if (_instance == null) {
                _instance = new SuperAwesomeAds();
            }
            return _instance;
        }
    }

	// private SABannerAd banner = null;

    // Start is called before the first frame update
    void Start()
    {
        AwesomeAds.init(true);
        loadAds();
    }

	// button actions
	public void loadAds () {
		SAInterstitialAd.setConfigurationStaging ();
		SAInterstitialAd.setOrientationPortrait ();
		SAInterstitialAd.enableParentalGate ();
		SAInterstitialAd.enableBackButton ();
		SAInterstitialAd.load (415);

		SAInterstitialAd.setCallback ((placementId, evt) => {
			switch (evt) {
			case SAEvent.adLoaded:{
				Debug.Log ("Ad loaded for " + placementId);
				break;
			}
			case SAEvent.adFailedToLoad:{ 
				Debug.Log ("adFailedToLoad for " + placementId);
				SAVideoAd.load (415);
				break;
			}
			case SAEvent.adShown: {
				Debug.Log ("adShown for " + placementId);
				SAVideoAd.load (415);
				break;
			}
			case SAEvent.adFailedToShow: {
				Debug.Log ("adFailedToShow for " + placementId);
				SAVideoAd.load (415);
				break;
			}
			case SAEvent.adClicked:break;
			case SAEvent.adClosed:{
				Debug.Log ("adClicked for " + placementId);
				SAVideoAd.load (415);
				break;
			}
			}
		});

		SAVideoAd.setConfigurationProduction ();
		SAVideoAd.enableTestMode ();
		SAVideoAd.enableCloseButton ();
		SAVideoAd.enableSmallClickButton ();
		SAVideoAd.disableBackButton ();
		SAVideoAd.setOrientationLandscape ();
		SAVideoAd.load (32848);

		SAVideoAd.setCallback ((placementId, evt) => {
			switch (evt) {
			case SAEvent.adLoaded:{
				Debug.Log ("Ad loaded for " + placementId);
				break;
			}
			case SAEvent.adFailedToLoad:{ 
				Debug.Log ("adFailedToLoad for " + placementId);
				SAVideoAd.load (32848);
				break;
			}
			case SAEvent.adShown: {
				Debug.Log ("adShown for " + placementId);
				SAVideoAd.load (32848);
				break;
			}
			case SAEvent.adFailedToShow: {
				Debug.Log ("adFailedToShow for " + placementId);
				SAVideoAd.load (32848);
				break;
			}
			case SAEvent.adClicked:break;
			case SAEvent.adClosed:{
				Debug.Log ("adClicked for " + placementId);
				SAVideoAd.load (32848);
				break;
			}
			}
		});

		// banner = SABannerAd.createInstance ();
		// banner.setConfigurationProduction ();
		// banner.enableTestMode ();
		// banner.disableParentalGate ();
		// banner.load (30989);
	}

	public void rewardedSAAds () {
		// Debug.Log("SAVideoAd.hasAdAvailable(32848) return value = " + SAVideoAd.hasAdAvailable(32848));
		if (SAVideoAd.hasAdAvailable(32848)) {
			SAVideoAd.play(32848);
		}
	}

	public void interstitialSAAds () {
		// Debug.Log("SAInterstitialAd.hasAdAvailable(415) return value = " + SAInterstitialAd.hasAdAvailable(415));
		// if (banner.hasAdAvailable()) {
		// 	banner.play();
		// }
		if (SAInterstitialAd.hasAdAvailable(415)) {
			SAInterstitialAd.play(415);
		}
	}		

	public bool rewardedAdIsLoaded() {
        return SAVideoAd.hasAdAvailable(32848);
    }

    public bool interstitialAdIsLoaded() {
        return SAInterstitialAd.hasAdAvailable(415);
    }

    public void rewardedAdShow() {
        SAVideoAd.play(32848);
    }

    public void interstitialAdShow() {
        SAInterstitialAd.play(415);
    }
}
